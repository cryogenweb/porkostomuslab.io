{:title "Live Clojure Cheat Sheet"
 :layout :post
 :klipse true
 :tags  ["Cryogen" "KLIPSE" "Cheat Sheet" "Clojure"]}
 
With all this crazy attention we've been giving to `juxt`, it seems wise to learn about the *other* Clojure functions too!
This page is intended to be a convenient springboard for a colossal interactive documentation station that will be put together over time.
Since it's running self-hosted Clojurescript, we'll be following [this cheat sheet](http://cljs.info/cheatsheet/) with examples from our good friend [ClojureDocs](http://clojuredocs.org/).
The killer is that it is *interactive*.
My hope is that it will serve as an extremely handy place where we can find what we need and immediately try stuff out, and when we see it *actually* evaluate, paste it into our programs!
Or at least, if nothing else, I might learn a couple things while doing this...

## Basics

### Define

```klipse-cljs
(def my-val 5)

my-val
```

```klipse-cljs
(defn foo [a b c]
	    (* a b c))
	    
(foo 1 2 3)
```

```klipse-cljs
(ns test)

(defn- foo []
         "World!")
         
(defn bar []
       (str "Hello " (foo)))
       
(foo)
```

```klipse-cljs
(bar)
```

```klipse-cljs
(ns playground)

(test/bar)
```

```
(test/foo)
```
(This will not work because `foo` is `private`)


```klipse-cljs
(let [a 1 b 2] 
         (+ a b))
```

```klipse-cljs
(letfn [(twice [x]
           (* x 2))
        (six-times [y]
           (* (twice y) 3))]
  (println "Twice 15 =" (twice 15))
  (println "Six times 15 =" (six-times 15)))
```

```klipse-cljs
(declare undefined-func)

(defn foo []
         (undefined-func))
```

That takes care of the **Define** section. In the next post we'll continue with **Branch**.