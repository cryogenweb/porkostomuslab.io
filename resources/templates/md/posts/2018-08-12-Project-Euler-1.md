{:title "Project Euler 1 - Multiples of 3 and 5"
 :layout :post
 :klipse true
 :tags  ["Project Euler" "KLIPSE" "coding exercises" "Clojure"]}
 
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

```klipse-cljs
(ns euler001
  (:require [cljs.test :refer-macros [deftest is run-tests]]))

(defn euler-001 [n]
  (->> (range n)
       (filter #(or (= 0 (rem % 3)) (= 0 (rem % 5))))
       (reduce +)))

(deftest euler-001-test
  (is (= 23 (euler-001 10))))
  
(run-tests)
```