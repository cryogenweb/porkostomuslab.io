{:title "Just Juxt #26: Half-truth (4clojure #83)"
 :layout :post
 :klipse true
 :tags  ["Cryogen" "KLIPSE" "juxt" "4clojure" "coding exercises"]}
 
![Half-truth](/img/truth.png)

>Write a function which takes a variable number of booleans. Your function should return true if some of the parameters are true, but not all of the parameters are true. Otherwise your function should return false.

```klipse-cljs
(ns live.test
  (:require [cljs.test :refer-macros [deftest is testing run-tests]]))
  
(defn half-truth [& col]
  (every? identity ((juxt some not-every?)
                    identity col)))

(deftest test-83
  (is (= false (half-truth false false)))
  (is (= true (half-truth true false)))
  (is (= false (half-truth true)))
  (is (= true (half-truth false true false)))
  (is (= false (half-truth true true true)))
  (is (= true (half-truth true true true false))))
  
(run-tests)
```