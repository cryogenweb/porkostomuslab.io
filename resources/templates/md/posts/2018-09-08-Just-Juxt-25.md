{:title "Just Juxt #25: Perfect Numbers (4clojure #80)"
 :layout :post
 :klipse true
 :tags  ["Cryogen" "KLIPSE" "juxt" "4clojure" "coding exercises"]}
 
![Perfect Numbers](/img/perfect.jpg)

>A number is "perfect" if the sum of its divisors equal the number itself. 6 is a perfect number because 1+2+3=6. Write a function which returns true for perfect numbers and false otherwise.

```klipse-cljs
(ns live.test
  (:require [cljs.test :refer-macros [deftest is testing run-tests]]))
  
(defn perfect-nums [x]
  (apply =
       ((juxt (comp (partial reduce +)
                    (partial apply filter)
                    (juxt (partial partial
                                   (comp zero? rem))
                      (partial range 1)))
          identity) x)))

(deftest perfect-nums-test
  (is (= (perfect-nums 6) true))
  (is (= (perfect-nums 7) false))
  (is (= (perfect-nums 496) true))
  (is (= (perfect-nums 500) false))
  (is (= (perfect-nums 8128) true)))
  
(run-tests)
```