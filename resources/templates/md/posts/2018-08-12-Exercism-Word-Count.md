{:title "Exercism - Word Count"
 :layout :post
 :klipse true
 :tags  ["Exercism" "KLIPSE" "coding exercises" "Clojure"]}
 
Given a phrase, count the occurrences of each word in that phrase.
For example for the input `olly olly in come free`:

```
olly: 2

in: 1

come: 1

free: 1
```

```klipse-cljs
(ns word-count
  (:require [cljs.test :refer-macros [deftest is run-tests]]
            [clojure.string :as str]))

(defn word-count [s]
  (->> s
       (str/lower-case)
       (re-seq #"\w+")
       (frequencies)))

(deftest count-one-word
  (is (= {"word" 1}
         (word-count/word-count "word"))))

(deftest count-one-of-each
  (is (= {"one" 1 "of" 1 "each" 1}
         (word-count/word-count "one of each"))))

(deftest count-multiple-occurrences
  (is (= {"one" 1 "fish" 4 "two" 1 "red" 1 "blue" 1}
         (word-count/word-count "one fish two fish red fish blue fish"))))

(deftest ignore-punctuation
  (is (= {"car" 1, "carpet" 1 "as" 1 "java" 1 "javascript" 1}
         (word-count/word-count "car : carpet as java : javascript!!&@$%^&"))))

(deftest include-numbers
  (is (= {"testing" 2 "1" 1 "2" 1}
         (word-count/word-count "testing, 1, 2 testing"))))

(deftest normalize-case
  (is (= {"go" 3}
         (word-count/word-count "go Go GO"))))
         
(run-tests)
```