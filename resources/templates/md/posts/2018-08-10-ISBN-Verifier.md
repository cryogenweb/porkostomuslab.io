{:title "Exercism - ISBN Verifier"
 :layout :post
 :klipse true
 :tags  ["Exercism" "KLIPSE" "coding exercises" "Clojure"]}

The [ISBN-10 verification process](https://en.wikipedia.org/wiki/International_Standard_Book_Number) is used to validate book identification numbers. These normally contain dashes and look like: `3-598-21508-8`

The ISBN-10 format is 9 digits (0 to 9) plus one check character (either a digit or an X only).
In the case the check character is an X, this represents the value '10'.
These may be communicated with or without hyphens, and can be checked for their validity by the following formula:

```
(x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9 * 2 + x10 * 1) mod 11 == 0
```

If the result is 0, then it is a valid ISBN-10, otherwise it is invalid.

Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula, and get:

```
(3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 * 1) mod 11 == 0
```

Since the result is 0, this proves that our ISBN is valid.

## Task

Given a string the program should check if the provided string is a valid ISBN-10.
Putting this into place requires some thinking about preprocessing/parsing of the string prior to calculating the check digit for the ISBN.
The program should be able to verify ISBN-10 both with and without separating dashes.

### Caveats

Converting from strings to numbers can be tricky in certain languages.
Now, it's even trickier since the check digit of an ISBN-10 may be 'X' (representing '10'). For instance `3-598-21507-X` is a valid ISBN-10.

### Bonus

* Generate a valid ISBN-13 from the input ISBN-10 (and maybe verify it again with a derived verifier).
* Generate valid ISBN, maybe even from a given starting ISBN.

## Source

[ISBN-10 check digit calculation](https://en.wikipedia.org/wiki/International_Standard_Book_Number#ISBN-10_check_digit_calculation)

```klipse-cljs
(ns isbn-verifier
  (:require [cljs.test :refer-macros [deftest is run-tests]]
            [clojure.string :as str]))

(defn parse [isbn]
  (map #(if (= \X %) 10 (js/parseInt % 10))
       (re-matches #"^\d{9}[\dX]$"
              (str/replace isbn "-" ""))))

(defn isbn? [isbn]
  (if (empty? (parse isbn))
    false
    (->> (parse isbn)
         (map #(* % %2) (range 10 0 -1))
         (apply +)
         (#(mod % 11))
         zero?)))
  
(deftest valid-isbn-number
  (is (= true (isbn? "3-598-21508-8"))))

(deftest invalid-isbn-check-digit
  (is (= false (isbn? "3-598-21508-9"))))

(deftest valid-isbn-number-with-a-check-digit-of-10
  (is (= true (isbn? "3-598-21507-X"))))

(deftest check-digit-is-a-character-other-than-X
  (is (= false (isbn? "3-598-21507-A"))))

(deftest invalid-character-in-isbn
  (is (= false (isbn? "3-598-2K507-0"))))

(deftest X-is-only-valid-as-a-check-digit
  (is (= false (isbn? "3-598-2X507-9"))))

(deftest valid-isbn-without-separating-dashes
  (is (= true (isbn? "3598215088"))))

(deftest isbn-without-separating-dashes-and-X-as-check-digit
  (is (= true (isbn? "359821507X"))))

(deftest isbn-without-check-digit-and-dashes
  (is (= false (isbn? "359821507"))))

(deftest too-long-isbn-and-no-dashes
  (is (= false (isbn? "3598215078X"))))

(deftest too-short-isbn
  (is (= false (isbn? "00"))))

(deftest isbn-without-check-digit
  (is (= false (isbn? "3-598-21507"))))

(deftest too-long-isbn
  (is (= false (isbn? "3-598-21507-XX"))))

(deftest check-digit-of-X-should-not-be-used-for-0
  (is (= false (isbn? "3-598-21515-X"))))
  
(run-tests)
```