{:title "Exercism Clojure Track - Rna Transcription"
 :layout :post
 :klipse true
 :tags  ["Exercism" "coding exercises" "Clojure"]}
 
Given a DNA strand, return its RNA complement (per RNA transcription).

Both DNA and RNA strands are a sequence of nucleotides.

The four nucleotides found in DNA are adenine (A), cytosine (C), guanine (G) and thymine (T).

The four nucleotides found in RNA are adenine (A), cytosine (C), guanine (G) and uracil (U).

Given a DNA strand, its transcribed RNA strand is formed by replacing each nucleotide with its complement:

* G -> C
* C -> G
* T -> A
* A -> U

Source: [Hyperphysics](http://hyperphysics.phy-astr.gsu.edu/hbase/Organic/transcription.html)

```klipse-cljs
(ns rna-transcription
  (:require [cljs.test :refer-macros [deftest is run-tests]]))

(defn to-rna [dna] ;; <- arglist goes here
  ;; your code goes here
)

(deftest transcribes-cytosine-to-guanine
  (is (= "G" (rna-transcription/to-rna "C"))))

(deftest transcribes-guanine-to-cytosine
  (is (= "C" (rna-transcription/to-rna "G"))))

(deftest transcribes-adenine-to-uracil
  (is (= "U" (rna-transcription/to-rna "A"))))

(deftest it-transcribes-thymine-to-adenine
  (is (= "A" (rna-transcription/to-rna "T"))))

(deftest it-transcribes-all-nucleotides
  (is (= "UGCACCAGAAUU" (rna-transcription/to-rna "ACGTGGTCTTAA"))))

(deftest it-validates-dna-strands
  (is (thrown? AssertionError (rna-transcription/to-rna "XCGFGGTDTTAA"))))
  
(run-tests)
```