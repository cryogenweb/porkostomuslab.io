{:title "Exercism Clojure Track - Bob"
 :layout :post
 :klipse true
 :tags  ["Exercism" "coding exercises" "Clojure"]}
 
Bob is a lackadaisical teenager. In conversation, his responses are very limited.

* Bob answers 'Sure.' if you ask him a question.
* He answers 'Whoa, chill out!' if you yell at him.
* He answers 'Calm down, I know what I'm doing!' if you yell a question at him.
* He says 'Fine. Be that way!' if you address him without actually saying anything.
* He answers 'Whatever.' to anything else.

Inspired by the 'Deaf Grandma' exercise in Chris Pine's Learn to Program tutorial: http://pine.fm/LearnToProgram/?Chapter=06

```klipse-cljs
(ns bob
  (:require [cljs.test :refer-macros [deftest is run-tests]]
            [clojure.string :as str]))

(defn response-for [s]
  (let [uc (and (= (str/upper-case s) s) (re-find #"[A-Z]+" s))
        q (= (last s) \?)]
    (cond (and q uc) "Calm down, I know what I'm doing!"
          uc "Whoa, chill out!"
          q "Sure."
          (empty? (str/trim s)) "Fine. Be that way!"
          :else "Whatever.")))

(deftest responds-to-something
  (is (= "Whatever." (bob/response-for "Tom-ay-to, tom-aaaah-to."))))

(deftest responds-to-shouts
  (is (= "Whoa, chill out!" (bob/response-for "WATCH OUT!"))))

(deftest responds-to-questions
  (is (= "Sure."
         (bob/response-for "Does this cryogenic chamber make me look fat?"))))

(deftest responds-to-forceful-talking
  (is (= "Whatever." (bob/response-for "Let's go make out behind the gym!"))))

(deftest responds-to-acronyms
  (is (= "Whatever."
         (bob/response-for "It's OK if you don't want to go to the DMV."))))

(deftest responds-to-forceful-questions
  (is (= "Calm down, I know what I'm doing!"
         (bob/response-for "WHAT THE HELL WERE YOU THINKING?"))))

(deftest responds-to-shouting-with-special-characters
  (is (= "Whoa, chill out!"
         (bob/response-for "ZOMG THE %^*@#$(*^ ZOMBIES ARE COMING!!11!!1!"))))

(deftest responds-to-shouting-numbers
  (is (= "Whoa, chill out!" (bob/response-for "1, 2, 3 GO!"))))

(deftest responds-to-shouting-with-no-exclamation-mark
  (is (= "Whoa, chill out!" (bob/response-for "I HATE YOU"))))

(deftest responds-to-statement-containing-question-mark
  (is (= "Whatever." (bob/response-for "Ending with ? means a question."))))

(deftest responds-to-silence
  (is (= "Fine. Be that way!" (bob/response-for ""))))

(deftest responds-to-prolonged-silence
  (is (= "Fine. Be that way!" (bob/response-for "    "))))

(deftest responds-to-only-numbers
  (is (= "Whatever." (bob/response-for "1, 2, 3"))))

(deftest responds-to-number-question
  (is (= "Sure." (bob/response-for "4?"))))

(run-tests)
```